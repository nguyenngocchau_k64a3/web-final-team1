<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="assets/css/device_edit.css" type="text/css">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>

<body>

    <?php
    $devide_name = "";
    $devide_description = "";
    ?>

    <form method="post" action="index.php" enctype="multipart/form-data">
        <div class="container">

            <div>
                Tên thiết bị
                <input type=" text" name="devide_name" value="<?php echo $devide_name; ?>">
            </div>

            <div>
                Mô tả chi tiết
                <input type="text" name="devide_description" rows="5" cols="40" value="<?php echo $devide_description; ?>">
            </div>

            <div>
                Avatar
                <div class="file-upload">
                    <div class="file-select">
                        <input type="file" name="chooseFile" id="chooseFile">
                        <div class="file-select-name" id="noFile"></div>
                        <div class="file-select-button" id="fileName" style="float:right">Choose File</div>
                    </div>
                </div>
            </div>
            <input type="submit" name="submit" value="Submit">
        </div>
    </form>
</body>

<script type="text/javascript">
    $('#chooseFile').bind('change', function() {
        var filename = $("#chooseFile").val();
        if (/^\s*$/.test(filename)) {
            $(".file-upload").removeClass('active');
            $("#noFile").text("No file chosen...");
        } else {
            $(".file-upload").addClass('active');
            $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
        }
    });
</script>